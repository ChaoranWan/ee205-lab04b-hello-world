///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04b - Hello World II
///
/// This is an Object Oriented C++ Hello World program
///
/// @file hello.cpp
/// @version 1.0
///
///////////////////////////////////////////////////////////////////////////////

#include <iostream>               // Replace <stdio.h> with <iostream>

int main() {                      // main() is still procedural -- no objects

   std::cout << "Hello World\n";  // We get cout from <iostream>.  It's the 
                                  // "character output" stream.

   return 0;
}

